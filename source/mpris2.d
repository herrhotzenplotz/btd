// MPRIS2 Media Player Control helpers

module mpris2;

import std.conv : to;

import ddbus;

alias PlayerName = string;

string[] GetDBusServices(ref Connection connection)
{
	auto obj = new PathIface(
		connection,
		"org.freedesktop.DBus",
		"/org/freedesktop/DBus",
		"org.freedesktop.DBus");

	return obj.ListNames().to!(string[]);
}

auto GetMPrisPlayers(ref Connection connection)
{
	import std.algorithm : filter, startsWith;
	auto Services = connection.GetDBusServices();
	return Services.filter!(Service => Service.startsWith("org.mpris.MediaPlayer2."));
}

auto GetPlayerProperty
	(string PropName, string EntryName, T = string)
	(ref Connection connection, const(PlayerName) Player)
{
	auto obj = new PathIface(
		connection,
		Player,
		"/org/mpris/MediaPlayer2",
		"org.freedesktop.DBus.Properties");

	auto dict = obj.Get("org.mpris.MediaPlayer2.Player", PropName).to!(Variant!DBusAny[string]);

	static if (is (T == string))
		return dict[EntryName].to!DBusAny.str;
	else
		static assert(false);
}

auto GetPlayerProperty
	(string PropName, T = string)
	(ref Connection connection, const(PlayerName) Player)
{
	auto obj = new PathIface(
		connection,
		Player,
		"/org/mpris/MediaPlayer2",
		"org.freedesktop.DBus.Properties");

	return obj.Get("org.mpris.MediaPlayer2.Player", PropName).to!T;
}

void PlayerInvokeMethod
	(string MethodName)
	(ref Connection connection, const(PlayerName) Player)
{
	auto obj = new PathIface(
		connection,
		Player,
		"/org/mpris/MediaPlayer2",
		"org.mpris.MediaPlayer2.Player");

	obj.call!DBusAny(MethodName);
}

alias PlayerGetCurrentTrack = GetPlayerProperty!("Metadata", "xesam:title");
alias PlayerGetCurrentTrackId = GetPlayerProperty!("Metadata", "xesam:trackid");
alias PlayerGetPlaybackStatus = GetPlayerProperty!("PlaybackStatus");
alias PlayerCanControl = GetPlayerProperty!("CanControl", bool);
alias PlayerGetVolume = GetPlayerProperty!("Volume", double);
alias PlayerPlayPause = PlayerInvokeMethod!"PlayPause";
alias PlayerPlay = PlayerInvokeMethod!"Play";
alias PlayerPause = PlayerInvokeMethod!"Pause";
alias PlayerStop = PlayerInvokeMethod!"Stop";
alias PlayerPrevious = PlayerInvokeMethod!"Previous";
alias PlayerNext = PlayerInvokeMethod!"Next";

