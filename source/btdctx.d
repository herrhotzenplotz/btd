module btdctx;

import ddbus;
import mpris2;

import std.array : array;
import std.range;
import std.typecons;

struct btdctx
{
	// DBus Session Bus Connection
	Connection Conn;

	alias Conn this;

	// Bluetooth Socket
	int Fd;

	// TODO: Really force an array?
	PlayerName[] KnownPlayers() @property
	{
		return this.Conn.GetMPrisPlayers.array;
	}

	Nullable!PlayerName CurrentPlayer() @property
	{
		auto Players = this.KnownPlayers();
		if (Players.empty())
			return Nullable!PlayerName.init;
		else
			return Nullable!PlayerName(Players.front);
	}
}

