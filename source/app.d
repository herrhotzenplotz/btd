version(FreeBSD):

import std.stdio;
import std.conv;
import std.getopt;
import std.string : toStringz;

import core.sys.posix.sys.socket;
import core.sys.posix.poll;
import core.sys.posix.unistd;
import core.sys.freebsd.bluetooth;
import core.sys.freebsd.err;

import core.stdc.string : memcpy;

import btdctx : btdctx;
import mpris2;

enum
{
	L2CAP_PSM_AVCTP = 0x0017,
}

struct avctp_header
{
	align(1):
	ubyte Id;
	ushort Pid;
}

struct avc_header
{
	align(1):

	ubyte Ctype;
	ubyte Subunit;
	ubyte Opcode;
	ubyte Operation;
	ubyte DataLength;
}

struct avrcp_header
{
	align(1):

	ubyte Ctype;
	ubyte Subunit;
	ubyte Opcode;
	ubyte[3] CompanyId;
	ubyte PduId;
	ubyte PacketType;
	ushort ParamLen;
}

enum
{
	AVRCP_CTYPE_CONTROL = 0x00,
	AVRCP_CTYPE_STATUS = 0x01,
	AVRCP_CTYPE_SPECIFIC_INQUIRY = 0x02,
	AVRCP_CTYPE_NOTIFY = 0x03,
	AVRCP_CTYPE_GENERAL_INQUIRY = 0x04,
	AVRCP_CTYPE_ACCEPTED = 0x09,
	AVRCP_CTYPE_REJECTED = 0x0a,
	AVRCP_CTYPE_STABLE = 0x0c,
	AVRCP_CTYPE_CHANGED = 0x0d,
	AVRCP_CTYPE_INTERIM = 0x0f,
}

enum
{
	AVRCP_PDUID_GETCAPABILITIES = 0x10,
	AVRCP_PDUID_REGISTERNOTIFICATION = 0x31,
}

enum
{
	AVRCP_OPCODE_VENDOR = 0x00,
	AVRCP_OPCODE_PASSTHRU = 0x7C,
}

enum
{
	AVRCP_EVENT_STATUS_CHANGED = 0x01,
	AVRCP_EVENT_TRACK_CHANGED = 0x02,
	AVRCP_EVENT_TRACK_REACHED_END = 0x03,
	AVRCP_EVENT_TRACK_REACHED_START = 0x04,
	AVRCP_EVENT_PLAYBACK_POS_CHANGED = 0x05,
	AVRCP_EVENT_SETTINGS_CHANGED = 0x08,
	AVRCP_EVENT_AVAILABLE_PLAYERS_CHANGED = 0x0a,
	AVRCP_EVENT_ADDRESSED_PLAYER_CHANGED = 0x0b,
	AVRCP_EVENT_VOLUME_CHANGED = 0x0d,
}

enum
{
	AVC_PLAY = 0x44,
	AVC_STOP = 0x45,
	AVC_PAUSE = 0x46,
	AVC_NEXT = 0x4b,
	AVC_PREV = 0x4c,
}

enum
{
	EVENT_PLAYBACK_STATUS_CHANGED = 0x01,
}

enum evmap = [ AVRCP_EVENT_STATUS_CHANGED: "Status Changed",
               AVRCP_EVENT_TRACK_CHANGED: "Track Changed",
               AVRCP_EVENT_TRACK_REACHED_END: "Track Reached End",
               AVRCP_EVENT_TRACK_REACHED_START: "Track Reached Start",
               AVRCP_EVENT_PLAYBACK_POS_CHANGED: "Playback Position Changed",
               AVRCP_EVENT_SETTINGS_CHANGED: "Settings Changed",
               AVRCP_EVENT_AVAILABLE_PLAYERS_CHANGED: "Available Players Changed",
               AVRCP_EVENT_ADDRESSED_PLAYER_CHANGED: "Adressed Players Changed",
               AVRCP_EVENT_VOLUME_CHANGED: "Volume Changed" ];

const(string) event_name(int ev)
{
	const(string) r = evmap[ev];
	if (r.length == 0)
		return "Unknown";
	else
		return r;
}

bool avctp_is_command(inout(avctp_header)* Hdr)
{
	return (Hdr.Id == 0x2) == 0;
}

bool avctp_is_response(inout(avctp_header)* Hdr)
{
	return !!(Hdr.Id == 0x2);
}

static ushort htobe16(ushort val)
{
	ubyte[2] buf;

	buf[0] = ((val >> 8) & 0xFF);
	buf[1] = (val & 0xFF);

	return *(cast(ushort *)buf.ptr);
}

struct bufreader
{
	const(ubyte) *buffer;
	size_t buffer_size;

	alias buffer this;

	size_t len() @property
	{
		return buffer_size;
	}

	ubyte opIndex(size_t i)
	{
		assert(i < buffer_size);
		return buffer[i];
	}

	size_t next_off = 0;
	T *Get(T)(size_t offset = 0)
	{
		assert(offset < buffer_size);
		assert((offset + T.sizeof) <= buffer_size);

		next_off = T.sizeof;

		return cast(T *)(buffer + offset);
	}

	T *Next(T)()
	{
		assert((next_off + T.sizeof) <= buffer_size);
		T* r = cast(T *)(buffer + next_off);
		next_off += T.sizeof;
		return r;
	}
}

void HandleSupportedEvent(ref btdctx Ctx, ubyte evt)
{
	ubyte[64] buffer;
	auto Reader = bufreader(buffer.ptr, buffer.sizeof);

	struct reg_evt_payload
	{
		align(1):
		ubyte evt_id;
		ubyte[4] rfa;
	}

	auto ctp_hdr = Reader.Get!avctp_header();
	auto rcp_hdr = Reader.Next!avrcp_header();
	auto rep = Reader.Next!reg_evt_payload();

	enum packet_len = (*ctp_hdr).sizeof + (*rcp_hdr).sizeof + (*rep).sizeof;
	static assert(packet_len <= buffer.sizeof, "buffer too smol");

	ctp_hdr.Pid = htobe16(0x110e);

	rcp_hdr.Ctype = AVRCP_CTYPE_NOTIFY;
	rcp_hdr.Subunit = (0x9 << 3) /* Subunit type */ | 0x0 /* Subunit id */;
	rcp_hdr.Opcode = 0; /* vendor specific */
	rcp_hdr.CompanyId[0] = 0x00;
	rcp_hdr.CompanyId[1] = 0x19;
	rcp_hdr.CompanyId[2] = 0x58;
	rcp_hdr.PduId = AVRCP_PDUID_REGISTERNOTIFICATION;
	rcp_hdr.PacketType = 0; /* single unfragmented */
	rcp_hdr.ParamLen = htobe16((*rep).sizeof);

	rep.evt_id = evt;

	auto n = Ctx.Fd.send(buffer.ptr, packet_len, 0);
	if (n < 0)
		err(1, "send");

	stderr.writeln("Registered event " ~ event_name(evt));
}

void HandleStableEvent(ref btdctx Ctx, ref bufreader Reader)
{
	auto ctp_hdr = Reader.Get!avctp_header();
	auto rcp_hdr = Reader.Next!avrcp_header();

	switch (rcp_hdr.PduId) {
	case AVRCP_PDUID_GETCAPABILITIES: {
		auto caps = *Reader.Next!ubyte();
		if (caps == 0x03) {
			auto n_evts = *Reader.Next!ubyte();

			for (ubyte i = 0; i < n_evts; ++i) {
				auto Evt = *Reader.Next!ubyte();
				Ctx.HandleSupportedEvent(Evt);
			}
		}
	} break;
	default: {
		stderr.writeln("unsupported pdu id");
	}
	}
}

void HandleChangeNotification(ref btdctx Ctx, ref bufreader Reader)
{
	auto ctp_hdr = Reader.Get!avctp_header();
	auto rcp_hdr = Reader.Next!avrcp_header();
	auto evt_id = *Reader.Next!ubyte();

	switch (evt_id) {
	case AVRCP_EVENT_VOLUME_CHANGED: {
		auto raw_vol = *Reader.Next!ubyte();
		raw_vol &= 0x7F; /* RFA */
		double perc = (cast(double)raw_vol / cast(double)0x7f) * 100.0;
		writeln("Volume changed to " ~ perc.to!string ~ "%");
	} break;
	default: {
		stderr.writeln("Unhandled AVRCP event");
	} break;
	}
}

void HandleResponse(ref btdctx Ctx, ref bufreader Reader)
{
	import std.format;

	auto ctp_hdr = Reader.Get!avctp_header();
	auto rcp_hdr = Reader.Next!avrcp_header();

	switch (rcp_hdr.Ctype) {
	case AVRCP_CTYPE_STABLE:
		Ctx.HandleStableEvent(Reader);
		break;
	case AVRCP_CTYPE_CHANGED:
		Ctx.HandleChangeNotification(Reader);
		break;
	case AVRCP_CTYPE_NOTIFY:
		stderr.writeln("NOTIFY response unhandled");
		break;
	default:
		stderr.writeln("unimplemented response 0x%x".format(rcp_hdr.Ctype));
		break;
	}
}

void ReplyPassthru(ref btdctx Ctx, const ubyte Ctype, const ubyte Operation)
{
	ubyte[8] buffer;
	auto Reader = bufreader(buffer.ptr, buffer.sizeof);

	auto ctp_hdr = Reader.Get!avctp_header();
	auto avc = Reader.Next!avc_header();
	enum pkt_len = (*ctp_hdr).sizeof + (*avc).sizeof;

	static assert(pkt_len <= buffer.sizeof);

	ctp_hdr.Id = 0x02;
	ctp_hdr.Pid = 0x110e.htobe16;

	avc.Ctype = Ctype;
	avc.Subunit = (0x9 << 3) /* Subunit type */ | 0x0 /* Subunit id */;
	avc.Opcode = AVRCP_OPCODE_PASSTHRU;
	avc.Operation = Operation;
	avc.DataLength = 0;

	auto rc = Ctx.Fd.send(buffer.ptr, pkt_len, 0);
	if (rc < 0)
		err(1, "send");
}

void HandlePassthru(ref btdctx Ctx, ref bufreader Reader)
{
	import std.format;

	auto ctp_hdr = Reader.Get!avctp_header();
	auto avc = Reader.Next!avc_header();
	auto Player = Ctx.CurrentPlayer;

	if (avc.Operation & 0x80) {
		stderr.writeln("Button released");
		return;
	}

	if (Player.isNull)
		goto reject;

	switch (avc.Operation & 0x7F) {
	case AVC_PLAY:
		Ctx.PlayerPlay(Player.get);
		goto ack;
	case AVC_PAUSE:
		Ctx.PlayerPause(Player.get);
		goto ack;
	case AVC_NEXT:
		Ctx.PlayerNext(Player.get);
		goto ack;
	case AVC_PREV:
		Ctx.PlayerPrevious(Player.get);
		goto ack;
	case AVC_STOP:
		Ctx.PlayerStop(Player.get);
		/* fallthru */
	ack:
		Ctx.ReplyPassthru(AVRCP_CTYPE_ACCEPTED, avc.Operation);
		return;
	default:
		stderr.writeln("rejecting unknown Operation: 0x%x".format(avc.Operation & 0x7F));
		break;
	}

reject:
	Ctx.ReplyPassthru(AVRCP_CTYPE_REJECTED, avc.Operation);
}

ubyte DBusPlayerStatusToAVRCPStatus(const(string) Status)
{
	enum ubyte[string] DBusPlayerStatusTable = [
		"Playing": 0x01,
		"Paused": 0x02,
		"Stopped": 0x00,
	];

	if (Status in DBusPlayerStatusTable)
		return DBusPlayerStatusTable[Status];
	else
		return 0xff; /* error */
}

void SendInterimResponse(ref btdctx Ctx, ubyte[] Param)
{
	ubyte[24] ResultBuffer = void;
	auto Reader = bufreader(ResultBuffer.ptr, ResultBuffer.sizeof);

	auto CtpHdr = Reader.Get!avctp_header();
	auto RcpHdr = Reader.Next!avrcp_header();
	ubyte *Payload = Reader.Next!ubyte();

	auto PacketLen = Param.length + (*CtpHdr).sizeof + (*RcpHdr).sizeof;

	/* FIXME: We'll potentially have to increase this buffer size */
	assert(PacketLen <= ResultBuffer.sizeof);

	CtpHdr.Id = 0x82;
	CtpHdr.Pid = htobe16(0x110e);

	RcpHdr.Ctype = AVRCP_CTYPE_INTERIM;
	RcpHdr.Subunit = (0x9 << 3) /* Subunit type */ | 0x0 /* Subunit id */;
	RcpHdr.Opcode = 0; /* vendor specific */
	RcpHdr.CompanyId[0] = 0x00;
	RcpHdr.CompanyId[1] = 0x19;
	RcpHdr.CompanyId[2] = 0x58;
	RcpHdr.PduId = AVRCP_PDUID_REGISTERNOTIFICATION;
	RcpHdr.PacketType = 0; /* single unfragmented */
	RcpHdr.ParamLen = htons(cast(ushort)Param.length);

	memcpy(Payload, Param.ptr, Param.length);

	auto n = Ctx.Fd.send(ResultBuffer.ptr, PacketLen, 0);
	if (n < 0)
		err(1, "send");
}

void HandleNotifyCommand(ref btdctx Ctx, ref bufreader Reader)
{
	import std.format;

	auto CtpHdr = Reader.Get!avctp_header();
	auto RcpHdr = Reader.Next!avrcp_header();
	auto EventId = *Reader.Next!ubyte();

	if (RcpHdr.ParamLen < 1) {
		stderr.writeln("NOTIFY command with bad param length %d".format(RcpHdr.ParamLen));
		return;
	}

	if (EventId != EVENT_PLAYBACK_STATUS_CHANGED) {
		stderr.writeln("NOTIFY command with bad event id");
		return;
	}

	assert(ntohs(RcpHdr.ParamLen) == 5);

	/* Query the current player status */
	ubyte PlayerStatus = 0xff;
	auto Player = Ctx.CurrentPlayer;
	if (!Player.isNull) {
		auto StatusString = Ctx.PlayerGetPlaybackStatus(Player.get);
		PlayerStatus = DBusPlayerStatusToAVRCPStatus(StatusString);
		debug writeln("player status: %s, 0x%02x".format(StatusString, PlayerStatus));
	} else {
		debug stderr.writeln("No active player?");
	}

	Ctx.SendInterimResponse([EventId, PlayerStatus]);
}

void HandleCommand(ref btdctx Ctx, ref bufreader Reader)
{
	import std.format;

	auto CtpHdr = Reader.Get!avctp_header();
	auto RcpHdr = Reader.Next!avrcp_header();

	if (RcpHdr.Ctype == AVRCP_CTYPE_CONTROL) {
		if (RcpHdr.Opcode == AVRCP_OPCODE_PASSTHRU) {
			Ctx.HandlePassthru(Reader);
		} else {
			stderr.writeln("unhandled control command: 0x%x".format(RcpHdr.Opcode));
		}
	} else if (RcpHdr.Ctype == AVRCP_CTYPE_NOTIFY) {
		if (RcpHdr.Opcode == AVRCP_OPCODE_VENDOR) {
			Ctx.HandleNotifyCommand(Reader);
		} else {
			stderr.writeln("NOTIFY without a vendor-specific opcode");
		}
	} else {
		stderr.writeln("Unhandled command with Ctype 0x%x".format(RcpHdr.Ctype));
	}
}

void HandleEvent(ref btdctx Ctx)
{
	ubyte[128] buffer;
	bufreader Reader;

	auto msgsize = Ctx.Fd.recv(buffer.ptr, buffer.sizeof, 0);
	if (msgsize < 0)
		err(1, "recv");

	Reader.buffer = buffer.ptr;
	Reader.buffer_size = buffer.sizeof;

	avctp_header *hdr = cast(avctp_header *)buffer.ptr;
	if (avctp_is_command(hdr))
		Ctx.HandleCommand(Reader);
	else if (avctp_is_response(hdr))
		Ctx.HandleResponse(Reader);
	else
		stderr.writeln("wat");
}

void QueryCaps(ref btdctx Ctx)
{
	ubyte[64] buffer;
	avctp_header *ctp_hdr = cast(avctp_header *)(buffer.ptr);
	avrcp_header *rcp_hdr = cast(avrcp_header *)(ctp_hdr + 1);
	ubyte *caps = cast(ubyte *)(rcp_hdr + 1);

	enum packet_len = (*ctp_hdr).sizeof + (*rcp_hdr).sizeof + (*caps).sizeof;
	static assert(packet_len <= buffer.sizeof);

	ctp_hdr.Pid = htobe16(0x110e);

	rcp_hdr.Ctype = AVRCP_CTYPE_STATUS;
	rcp_hdr.Subunit = (0x9 << 3) /* Subunit type */ | 0x0 /* Subunit id */;
	rcp_hdr.Opcode = 0; /* vendor */
	rcp_hdr.CompanyId[0] = 0x00;
	rcp_hdr.CompanyId[1] = 0x19;
	rcp_hdr.CompanyId[2] = 0x58;
	rcp_hdr.PduId = AVRCP_PDUID_GETCAPABILITIES;
	rcp_hdr.PacketType = 0; /* single unfragmented */
	rcp_hdr.ParamLen = htobe16(cast(ushort)((*caps).sizeof));

	caps[0] = 0x3; /* query supported events */

	auto n = Ctx.Fd.send(buffer.ptr, packet_len, 0);
	if (n < 0)
		err(1, "send");
}

void MainLoop(ref btdctx Ctx)
{
	Ctx.QueryCaps;

	for (;;) {
		auto pfd = pollfd(Ctx.Fd, POLLIN|POLLHUP, 0);

		if (poll(&pfd, 1, -1) < 0)
			err(1, "poll");

		if (pfd.revents & POLLIN)
			Ctx.HandleEvent;
	}
}

void BluetoothSetup(ref btdctx Ctx, const(string) Hostname)
{
	Ctx.Fd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BLUETOOTH_PROTO_L2CAP);
	char[2*6+(6-1)] devname;
	sockaddr_l2cap sa, lsa;

	hostent *l = bt_gethostbyname(Hostname.toStringz);
	if (l == null)
		err(1, "bt_gethostbyname");

	lsa.l2cap_len = lsa.sizeof;
	lsa.l2cap_family = AF_BLUETOOTH;
	if (Ctx.Fd.bind(cast(sockaddr *)&lsa, lsa.sizeof) < 0)
		err(1, "bind");

	memcpy(&sa.l2cap_bdaddr, l.h_addr_list[0], sa.l2cap_bdaddr.sizeof);
	bt_ntoa(&sa.l2cap_bdaddr, devname.ptr);
	writeln("Address of headphones is " ~ devname);

	sa.l2cap_len = sa.sizeof;
	sa.l2cap_family = AF_BLUETOOTH;
	sa.l2cap_psm = L2CAP_PSM_AVCTP;

	if (Ctx.Fd.connect(cast(sockaddr *)&sa, sa.sizeof) < 0)
		err(1, "connect");
}

void BluetoothTeardown(ref btdctx Ctx)
{
	Ctx.Fd.close();
}

int main(string[] args)
{
	import ddbus : connectToBus;

	auto Context = btdctx(connectToBus(), -1);

	Context.BluetoothSetup("MOMENTUM_4");
	Context.MainLoop;
	Context.BluetoothTeardown;

	return 42;
}
